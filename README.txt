Framework overview

this framework is build as a Maven Java 1.8 project and uses Selenium to test the UX. The test execution is handled by TestNG and the reporting
is done in ReportNG and ExtentReports. 


Structure

This testing strategy uses page object factories. In our case we have 3 pages in the application:
Landing page
add computer page
edit computer page

Each of these pages have a set of actions which describe what the user can do in the particular page and a set of locators for all the elements 
we need in that page. Action pages extend the page base class.
Action classes are placed in the assignment/pages/actions package
Locator classes are placed in the assignment/pages/locators package



Execution

The tests can be executed in two ways:
As a TestNG suite, by right clicking the Demo/src/test/resources/runner/computerDBTests.xml and select to run as TestNG suite.
As Maven build, by right clicking the pom.xml and run as Maven clean first and Maven test second. The reports for the maven execution
can be found in the following folders:
/Demo/target/surefire-reports/html/index.html for reportNG 
/Demo/target/surefire-reports/html/extent.html for extentReports 
Note: the build is going to fail as there are 4 tests that are failing their assertions. Details and screenshots are included in the reports.
The tests are executed in the order that is described in the computerDBTests.xml file


Tests

Tests can be found in the assignment/testcases/compdb package and are divided in 3 subpackages: CRUD, pagination and sorting
Tests are designed to run independently from each other, therefore they create their own test data.
Some tests may appear similar (i.e CRUD tests), but the difference is in their assertions.



Logging

The framework records application and selenium logs using log4j. 
the logs are located under:
/Demo/src/test/resources/logs/Application.log
/Demo/src/test/resources/logs/Selenium.log


