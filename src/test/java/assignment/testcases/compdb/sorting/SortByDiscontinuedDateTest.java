package assignment.testcases.compdb.sorting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class SortByDiscontinuedDateTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test
	public void sortByDiscontinuedDateTest() throws InterruptedException
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		
		
		int initial = landingPageActions.retrieveNumberOfComputers();
		
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha31");
		addComputerPageActions.inputIntroducedDate("1999-01-01");
		addComputerPageActions.inputDiscontinuedDate("1999-02-02");
		addComputerPageActions.selectCompany("ASUS");
		addComputerPageActions.createComputer();
		
	
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha32");
		addComputerPageActions.inputIntroducedDate("2000-01-01");
		addComputerPageActions.inputDiscontinuedDate("2000-02-02");
		addComputerPageActions.selectCompany("IBM");
		addComputerPageActions.createComputer();

		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == initial+2);
		
		landingPageActions.filterByName("Alpha3");
		
		landingPageActions.search();
		
		
		Assert.assertTrue(landingPageActions.retrieveFirstDiscontinuedDate().equals("02 Feb 1999"),"Verifying that the first discontinued date is 02 Feb 1999");
		
		Assert.assertTrue(landingPageActions.retrieveSecondDiscontinuedDate().equals("02 Feb 2000"),"Verifying that the second discontinued date is 02 Feb 2000");
		
		
		landingPageActions.invertSortingByDiscontinuedDate();
		
		Assert.assertTrue(landingPageActions.retrieveFirstDiscontinuedDate().equals("02 Feb 2000"),"Verifying that the first discontinued date is 02 Feb 2000");
		
		Assert.assertTrue(landingPageActions.retrieveSecondDiscontinuedDate().equals("02 Feb 1999"),"Verifying that the second discontinued date is 02 Feb 1999");
		
		
		
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
