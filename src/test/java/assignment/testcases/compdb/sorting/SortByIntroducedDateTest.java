package assignment.testcases.compdb.sorting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class SortByIntroducedDateTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test
	public void sortByIntroducedDateTest() throws InterruptedException
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		
		
		int initial = landingPageActions.retrieveNumberOfComputers();
		
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha21");
		addComputerPageActions.inputIntroducedDate("1999-01-01");
		addComputerPageActions.inputDiscontinuedDate("1999-02-02");
		addComputerPageActions.selectCompany("ASUS");
		addComputerPageActions.createComputer();
		
	
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha22");
		addComputerPageActions.inputIntroducedDate("2000-01-01");
		addComputerPageActions.inputDiscontinuedDate("2000-02-02");
		addComputerPageActions.selectCompany("IBM");
		addComputerPageActions.createComputer();

		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == initial+2);
		
		landingPageActions.filterByName("Alpha2");
		
		landingPageActions.search();
		
		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == 2,"Verifying that the search wields 2 results");
		
		Assert.assertTrue(landingPageActions.retrieveFirstIntroducedDate().equals("01 Jan 1999"),"Verifying that the first introduction date is 01 Jan 1999");
		
		Assert.assertTrue(landingPageActions.retrieveSecondIntroducedDate().equals("01 Jan 2000"),"Verifying that the second introduction date is 01 Jan 2000");
		
		
		landingPageActions.invertSortingByIntroductionDate();
		
		Assert.assertTrue(landingPageActions.retrieveFirstIntroducedDate().equals("01 Jan 2000"),"Verifying that the first introduction date is 01 Jan 2000");
		
		Assert.assertTrue(landingPageActions.retrieveSecondIntroducedDate().equals("01 Jan 1999"),"Verifying that the second introduction date is 01 Jan 1999");
		
		
		
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
