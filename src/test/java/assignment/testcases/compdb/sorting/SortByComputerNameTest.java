package assignment.testcases.compdb.sorting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import assignment.base.Page;
import assignment.errorcollectors.ErrorCollector;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.EditComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class SortByComputerNameTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test
	public void sortByComputerNameTest() throws InterruptedException
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		EditComputerPageActions editComputerPageActions = new EditComputerPageActions();
		
		
	
		
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha51");
		addComputerPageActions.inputIntroducedDate("1999-01-01");
		addComputerPageActions.inputDiscontinuedDate("1999-02-02");
		addComputerPageActions.selectCompany("ASUS");
		addComputerPageActions.createComputer();
		
	
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha52");
		addComputerPageActions.inputIntroducedDate("2000-01-01");
		addComputerPageActions.inputDiscontinuedDate("2000-02-02");
		addComputerPageActions.selectCompany("IBM");
		addComputerPageActions.createComputer();

		
		
		landingPageActions.filterByName("Alpha5");
		
		landingPageActions.search();
		
		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == 2, "Verifying that the search yields two results");
		
		Assert.assertTrue(landingPageActions.retrieveFirstResultName().equals("Alpha51"),"Verifying that the first computer name is: Alpha51");
	
		
		Assert.assertTrue(landingPageActions.retrieveSecondResultName().equals("Alpha52"),"Verifying that the second computer name is: Alpha52");
		
		
		landingPageActions.invertSortingByComputerName();
		
		Assert.assertTrue(landingPageActions.retrieveFirstResultName().equals("Alpha52"),"Verifying that the first computer name is: Alpha52");
		
		Assert.assertTrue(landingPageActions.retrieveSecondResultName().equals("Alpha51"),"Verifying that the second computer name is: Alpha51");
		
	
//		
//		//cleaning up the data
//		
//		landingPageActions.editComputer("Alpha1");
//		
//		editComputerPageActions.deleteComputer();
//		
//		landingPageActions.filterByName("Alpha");
//		
//		landingPageActions.search();
//		
//		landingPageActions.editComputer("Alpha2");
//	
//		editComputerPageActions.deleteComputer();
//		
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
