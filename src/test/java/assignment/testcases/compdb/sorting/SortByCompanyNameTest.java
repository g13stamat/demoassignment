package assignment.testcases.compdb.sorting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class SortByCompanyNameTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test
	public void sortByCompanyNameTest() throws InterruptedException
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		
		
		int initial = landingPageActions.retrieveNumberOfComputers();
		
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha41");
		addComputerPageActions.inputIntroducedDate("1999-01-01");
		addComputerPageActions.inputDiscontinuedDate("1999-02-02");
		addComputerPageActions.selectCompany("ASUS");
		addComputerPageActions.createComputer();
		
	
		landingPageActions.addComputer();
		addComputerPageActions.inputComputerName("Alpha42");
		addComputerPageActions.inputIntroducedDate("2000-01-01");
		addComputerPageActions.inputDiscontinuedDate("2000-02-02");
		addComputerPageActions.selectCompany("IBM");
		addComputerPageActions.createComputer();

		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == initial+2);
		
		landingPageActions.filterByName("Alpha4");
		
		landingPageActions.search();
		
		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == 2,"Verifying that the search yields two results");
		
		Assert.assertTrue(landingPageActions.retrieveFirstCompany().contains("ASUS"),"Verifying that the first company is ASUS");
		
		Assert.assertTrue(landingPageActions.retrieveSecondCompany().contains("IBM"),"Verifying that the second company is IBM");
		
		
		landingPageActions.invertSortingByIntroductionDate();
		
		Assert.assertTrue(landingPageActions.retrieveFirstResultName().contains("IBM"),"Verifying that the first company is IBM");
		
		Assert.assertTrue(landingPageActions.retrieveSecondResultName().contains("ASUS"),"Verifying that the second company is ASUS");
		
		
		
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
