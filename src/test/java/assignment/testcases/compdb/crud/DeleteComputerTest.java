package assignment.testcases.compdb.crud;

import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.EditComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class DeleteComputerTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test(dataProviderClass=assignment.utilities.Utilities.class,dataProvider="dp")
	public void createComputerTest(Hashtable<String,String> data)
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		EditComputerPageActions editComputerPageActions = new EditComputerPageActions();
		
		int initial = landingPageActions.retrieveNumberOfComputers();
		
		landingPageActions.addComputer();		
		addComputerPageActions.newComputer(data.get("name"), data.get("introducedDate"), data.get("discontinuedDate"), data.get("company"));
		addComputerPageActions.createComputer();
		
		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == initial+1,"Asserting that the number of computers is increased by 1");
		
		landingPageActions.filterByName(data.get("name"));
		landingPageActions.search();
		
		String toDelete = landingPageActions.retrieveFirstResultName();
		
		landingPageActions.editComputer(toDelete);
		
		editComputerPageActions.deleteComputer();
		
		Assert.assertTrue(landingPageActions.retrieveNumberOfComputers() == initial,"Asserting that the number of computers is reduced by 1");

		
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
