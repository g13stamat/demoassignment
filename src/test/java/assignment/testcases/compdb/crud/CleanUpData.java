package assignment.testcases.compdb.crud;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.EditComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class CleanUpData {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test
	public void cleanUpData() throws InterruptedException
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		EditComputerPageActions editComputerPageActions = new EditComputerPageActions();
		
		
		
		int count = landingPageActions.retrieveNumberOfComputers();
			
			
			while(count>1)
			{
				landingPageActions.filterByName("Alpha");
				landingPageActions.search();
				
				String tobeDeleted = landingPageActions.retrieveFirstResultName();
				landingPageActions.editComputer(tobeDeleted);
				editComputerPageActions.deleteComputer();
				count--;
				if(count==1)
				{
					System.out.println("1 result left");
				}
			}
		
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
