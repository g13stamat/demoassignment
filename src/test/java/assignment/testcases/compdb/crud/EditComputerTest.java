package assignment.testcases.compdb.crud;

import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.EditComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class EditComputerTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test(dataProviderClass=assignment.utilities.Utilities.class,dataProvider="dp")
	public void editComputerTest(Hashtable<String,String> data)
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		EditComputerPageActions editComputerPageActions = new EditComputerPageActions();
		
		int initial = landingPageActions.retrieveNumberOfComputers();
		
		landingPageActions.addComputer();		
		addComputerPageActions.newComputer(data.get("name"), data.get("introducedDate"), data.get("discontinuedDate"), data.get("company"));
		addComputerPageActions.createComputer();
		
		landingPageActions.filterByName(data.get("name"));
		
		landingPageActions.search();
		
		String tobeEdited = landingPageActions.retrieveFirstResultName();
		
		landingPageActions.editComputer(tobeEdited);
		
		editComputerPageActions.inputComputerName(data.get("newName"));
		editComputerPageActions.inputIntroducedDate(data.get("newIntroducedDate"));
		editComputerPageActions.inputDiscontinuedDate(data.get("newDiscontinuedDate"));
		editComputerPageActions.selectCompany(data.get("newCompany"));
		editComputerPageActions.saveComputer();
		
		landingPageActions.filterByName(data.get("newName"));
		landingPageActions.search();
		
		String edited = landingPageActions.retrieveFirstResultName();
		
		
		landingPageActions.editComputer(edited);
		
		Assert.assertTrue(editComputerPageActions.readComputerName().equals(data.get("newName")),"Asserting that the new computer name is displayed");
		
		Assert.assertTrue(editComputerPageActions.readIntroducedDate().equals(data.get("newIntroducedDate")),"Asserting that the new introduced date is displayed");
		
		Assert.assertTrue(editComputerPageActions.readDiscontinuedDate().equals(data.get("newDiscontinuedDate")),"Asserting that the new discontinued date is displayed");
		System.out.println("company is "+editComputerPageActions.readCompany());
		Assert.assertTrue(editComputerPageActions.readCompany().equals(data.get("newCompany")),"Asserting that the new company name is displayed");
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
