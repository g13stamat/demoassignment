package assignment.testcases.compdb.crud;

import static org.testng.Assert.assertTrue;

import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.EditComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class ReadComputerTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test(dataProviderClass=assignment.utilities.Utilities.class,dataProvider="dp")
	public void readComputerTest(Hashtable<String,String> data)
	{
		LandingPageActions landingPageActions = new LandingPageActions();
		AddComputerPageActions addComputerPageActions = new AddComputerPageActions();
		EditComputerPageActions editComputerPageActions = new EditComputerPageActions();
		
		landingPageActions.addComputer();		
		addComputerPageActions.newComputer(data.get("name"), data.get("introducedDate"), data.get("discontinuedDate"), data.get("company"));
		addComputerPageActions.createComputer();
		
		landingPageActions.filterByName(data.get("name"));
		landingPageActions.search();
		
		String toRead = landingPageActions.retrieveFirstResultName();
		
		landingPageActions.editComputer(toRead);
		Assert.assertTrue(editComputerPageActions.readComputerName().equals(data.get("name")),"Asserting that the name matches the file");
		Assert.assertTrue(editComputerPageActions.readIntroducedDate().equals(data.get("introducedDate")),"Asserting that the introduced date matches the file");
		Assert.assertTrue(editComputerPageActions.readDiscontinuedDate().equals(data.get("discontinuedDate")),"Asserting that the discontinued date matches the file");
		Assert.assertTrue(editComputerPageActions.readCompany().equals(data.get("company")));
		
			
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
