package assignment.testcases.compdb.pagination;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import assignment.base.Page;
import assignment.pages.actions.AddComputerPageActions;
import assignment.pages.actions.LandingPageActions;


public class LeftPaginationTest {
	
	@BeforeMethod
	public void setup()
	{
		
		Page.initConfiguration();
	}
	
	
	
	@Test
	public void leftPaginationTest() throws InterruptedException
	{
		LandingPageActions landingPageActions = new LandingPageActions();
	
		int initial = landingPageActions.retrieveNumberOfComputers();
		String currentPagination = landingPageActions.retrieveCurrentPageText();
				
		
		landingPageActions.goToNextResultPage();
		
		Assert.assertTrue(landingPageActions.retrieveCurrentPageText().equals("Displaying 11 to 20 of "+initial),"Asserting that the left pagination works");
		
		landingPageActions.goToPreviousResultPage();
		
		Assert.assertTrue(landingPageActions.retrieveCurrentPageText().equals("Displaying 1 to 10 of "+initial),"Asserting that the right pagination works");
		
	
	}
	
	@AfterMethod
	public void teardown()
	{
		Page.quitBrowser();
	}

}
