package rough;

import assignment.pages.actions.EditComputerPageActions;
import assignment.pages.actions.LandingPageActions;

public class Cleanup {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LandingPageActions landingPageActions = new LandingPageActions();
		EditComputerPageActions editComputerPageActions = new EditComputerPageActions();
		
		
		int count = landingPageActions.retrieveNumberOfComputers();
	
			
			
			while(count>3)
			{
				landingPageActions.filterByName("Alpha");
				landingPageActions.search();
				
				String tobeDeleted = landingPageActions.retrieveFirstResultName();
				landingPageActions.editComputer(tobeDeleted);
				editComputerPageActions.deleteComputer();
				count--;
			}
			landingPageActions.filterByName("Alpha");
			landingPageActions.search();
			
			String tobeDeleted = landingPageActions.retrieveFirstResultName();
			landingPageActions.editComputer(tobeDeleted);

		
		
	}

}
