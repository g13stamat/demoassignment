package assignment.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LandingPageLocators {
	
	
	@FindBy(id ="searchbox")
	public WebElement searchField;
	
	@FindBy(id ="searchsubmit")
	public WebElement filterBtn;

	
	@FindBy(xpath =".//*[@id='main']/table")
	public WebElement mainTable;
	
	
	@FindBy(id ="add")
	public WebElement addBtn;
	
	@FindBy(linkText ="Computer name")
	public WebElement sortByComputerNameLnk;
	
	@FindBy(linkText ="Introduced")
	public WebElement sortByDateIntroduced;
	
	@FindBy(linkText ="Discontinued")
	public WebElement sortByDateDiscontinued;
	
	@FindBy(linkText ="Company")
	public WebElement sortByDateCompanyName;
	
	@FindBy(partialLinkText ="Next")
	public WebElement nextPageLnk;
	
	@FindBy(partialLinkText ="Previous")
	public WebElement previousPageLnk;

	@FindBy(xpath ="//div[@class='alert-message warning']")
	public WebElement deleteMessage;
	
	@FindBy(xpath =".//*[@id='main']/h1")
	public WebElement numberOfComputers;
	
	@FindBy(xpath =".//*[@id='pagination']/ul/li[2]/a")
	public WebElement currentPageText;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[1]/td[1]")
	public WebElement computerNameResult1;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[2]/td[1]")
	public WebElement computerNameResult2;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[1]/td[2]")
	public WebElement introducedDateResult1;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[2]/td[2]")
	public WebElement introducedDateResult2;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[1]/td[3]")
	public WebElement discontinuedDateResult1;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[2]/td[3]")
	public WebElement discontinuedDateResult2;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[1]/td[4]")
	public WebElement companyResult1;
	
	@FindBy(xpath =".//*[@id='main']/table/tbody/tr[2]/td[4]")
	public WebElement companyResult2;
	
	
}
	
