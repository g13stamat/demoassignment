package assignment.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class EditComputerPageLocators {
	
	
	@FindBy(xpath =".//*[@id='main']/form/fieldset/div[1]")
	public WebElement computerNameTab;
	
	@FindBy(xpath =".//*[@id='main']/form/fieldset/div[2]")
	public WebElement introducedDateTab;

	
	@FindBy(xpath =".//*[@id='main']/form/fieldset/div[2]")
	public WebElement discontinuedDateTab;
	
	
	@FindBy(id ="name")
	public WebElement computerName;
	
	@FindBy(id ="introduced")
	public WebElement introducedDateField;
	
	@FindBy(id ="discontinued")
	public WebElement discontinuedDateField;
	
	@FindBy(id ="company")
	public WebElement companyDropDown;
	
	
	public Select selectCompany()
	{
		return new Select(companyDropDown);
	}
	
	@FindBy(xpath ="//input[@value='Save this computer']")
	public WebElement saveComputerBtn;
	
	@FindBy(linkText ="Cancel")
	public WebElement cancelBtn;
	
	@FindBy(xpath ="//input[@value='Delete this computer']")
	public WebElement deleteComputerBtn;
	
	
}
