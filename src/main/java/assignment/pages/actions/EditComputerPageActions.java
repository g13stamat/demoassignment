package assignment.pages.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

import assignment.base.Page;
import assignment.pages.locators.EditComputerPageLocators;




public class EditComputerPageActions extends Page{
	
	public EditComputerPageLocators editComputerPageLocators;
	
	
	public EditComputerPageActions()
	{
		this.editComputerPageLocators = new EditComputerPageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.editComputerPageLocators);
		log.debug("Initialized EditComputerPageLocators page");
		test.log(LogStatus.INFO, "Initialized EditComputerPageLocators page");
		
	}
	
	public void editComputer()
	{
		
	}
	
	public void inputComputerName(String computerName)
	{
		resetElement(editComputerPageLocators.computerName);
		editComputerPageLocators.computerName.sendKeys(computerName);
		log.debug("Entering name: "+computerName);
		test.log(LogStatus.INFO, "Entering name: "+computerName);
		
	}
	
	public void inputIntroducedDate(String introDate)
	{
		resetElement(editComputerPageLocators.introducedDateField);
		editComputerPageLocators.introducedDateField.sendKeys(introDate);
		log.debug("Entering introduced date: "+introDate);
		test.log(LogStatus.INFO, "Entering introduced date: "+introDate);
		
	}
	
	
	public void inputDiscontinuedDate(String discoDate)
	{
		resetElement(editComputerPageLocators.discontinuedDateField);
		editComputerPageLocators.discontinuedDateField.sendKeys(discoDate);
		log.debug("Entering discontinued date: "+discoDate);
		test.log(LogStatus.INFO, "Entering discontinued date: "+discoDate);
		
	}
	
	public void selectCompany(String company)
	{
		
		editComputerPageLocators.selectCompany().selectByVisibleText(company);
		log.debug("Selected company: "+company);
		test.log(LogStatus.INFO, "Selected company: "+company);
		
	}
	
	
	
	public void saveComputer()
	{
		
		editComputerPageLocators.saveComputerBtn.click();
		log.debug("Click on the Add new computer button");
		test.log(LogStatus.INFO, "Click on the Add new computer button");
	}
	
	
	public void deleteComputer()
	{
		
		editComputerPageLocators.deleteComputerBtn.click();
		log.debug("Click on the Delete computer button");
		test.log(LogStatus.INFO, "Click on the Delete computer button");
	}
	
	
	public String retrieveNameTabClass()
	{
		String nameClass = editComputerPageLocators.computerNameTab.getAttribute("class");
		return nameClass;
	}
	
	public String readComputerName()
	{
		String computerName = editComputerPageLocators.computerName.getAttribute("value");
		
		return computerName;
		
	}
	
	
	
	public String readIntroducedDate()
	{
		
		String readIntroducedDate = editComputerPageLocators.introducedDateField.getAttribute("value");
		
		return readIntroducedDate;
	}
	
	
	public String readDiscontinuedDate()
	{
		String readDiscontinuedDate = editComputerPageLocators.discontinuedDateField.getAttribute("value");
		
		return readDiscontinuedDate;
		
	}
	
	
	public String readCompany()
	{
		String readCompany = editComputerPageLocators.selectCompany().getFirstSelectedOption().getText();
		
		return readCompany;
		
	}
	
	
	
}
