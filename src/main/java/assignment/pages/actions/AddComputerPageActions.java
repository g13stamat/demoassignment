package assignment.pages.actions;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import assignment.base.Page;
import assignment.pages.locators.AddComputerPageLocators;




public class AddComputerPageActions extends Page{
	
	public AddComputerPageLocators addComputerPageLocators;
	
	
	public AddComputerPageActions()
	{
		this.addComputerPageLocators = new AddComputerPageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.addComputerPageLocators);
		log.debug("Initialized AddComputerPageActions page");
		test.log(LogStatus.INFO, "Initialized AddComputerPageActions page");
		
	}
	
	
	public void newComputer(String computerName, String introDate, String discoDate, String company)
	{
		resetElement(addComputerPageLocators.computerName);
		inputComputerName(computerName);
//		addComputerPageLocators.computerName.sendKeys(computerName);
//		log.debug("Entering name: "+computerName);
//		test.log(LogStatus.INFO, "Entering name: "+computerName);
//		Reporter.log("Entering name: "+computerName);
		resetElement(addComputerPageLocators.introducedDateField);
		addComputerPageLocators.introducedDateField.sendKeys(introDate);
		log.debug("Entering introduced date: "+introDate);
		test.log(LogStatus.INFO, "Entering introduced date: "+introDate);
		Reporter.log("Entering introduced date: "+introDate);
		resetElement(addComputerPageLocators.discontinuedDateField);
		addComputerPageLocators.discontinuedDateField.sendKeys(discoDate);
		log.debug("Entering discontinued date: "+discoDate);
		test.log(LogStatus.INFO, "Entering discontinued date: "+discoDate);
		Reporter.log("Entering discontinued date: "+discoDate);
		addComputerPageLocators.selectCompany().selectByVisibleText(company);
		log.debug("Selected company: "+company);
		test.log(LogStatus.INFO, "Selected company: "+company);
		Reporter.log("Selected company: "+company);
		
	}
	
	public void inputComputerName(String computerName)
	{
		resetElement(addComputerPageLocators.computerName);
		addComputerPageLocators.computerName.sendKeys(computerName);
		log.debug("Entering name: "+computerName);
		test.log(LogStatus.INFO, "Entering name: "+computerName);
		
	}
	
	public void inputIntroducedDate(String introDate)
	{
		resetElement(addComputerPageLocators.introducedDateField);
		addComputerPageLocators.introducedDateField.sendKeys(introDate);
		log.debug("Entering introduced date: "+introDate);
		test.log(LogStatus.INFO, "Entering introduced date: "+introDate);
		
	}
	
	
	public void inputDiscontinuedDate(String discoDate)
	{
		resetElement(addComputerPageLocators.discontinuedDateField);
		addComputerPageLocators.discontinuedDateField.sendKeys(discoDate);
		log.debug("Entering discontinued date: "+discoDate);
		test.log(LogStatus.INFO, "Entering discontinued date: "+discoDate);
		
	}
	
	public void selectCompany(String company)
	{
		
		addComputerPageLocators.selectCompany().selectByVisibleText(company);
		log.debug("Selected company: "+company);
		test.log(LogStatus.INFO, "Selected company: "+company);
		
	}
	
	
	
	public void createComputer()
	{
		
		addComputerPageLocators.createComputerBtn.click();
		log.debug("Click on the Create this computer button");
		test.log(LogStatus.INFO, "Click on the Create this computer button");
		Reporter.log("Click on the Create this computer button");
	}
	
	
	public String retrieveNameTabClass()
	{
		String nameClass = addComputerPageLocators.computerNameTab.getAttribute("class");
		return nameClass;
	}
	
	
	public String retrieveIntroDateTabClass()
	{
		String nameClass = addComputerPageLocators.introducedDateTab.getAttribute("class");
		return nameClass;
	}
	
	public String retrieveDiscoDateTabClass()
	{
		String nameClass = addComputerPageLocators.discontinuedDateTab.getAttribute("class");
		return nameClass;
	}
	
	
}
