package assignment.pages.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import assignment.base.Page;
import assignment.pages.locators.LandingPageLocators;



public class LandingPageActions extends Page{
	
	public LandingPageLocators landingPageLocators;
	
	
	public LandingPageActions()
	{
		this.landingPageLocators = new LandingPageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.landingPageLocators);
		log.debug("Initialized LandingPageActions page");
		test.log(LogStatus.INFO, "Initialized LandingPageActions page");
		Reporter.log("Initialized LandingPageActions page");
	}
	
	public void filterByName(String computerName)
	{
		resetElement(landingPageLocators.searchField);
		landingPageLocators.searchField.sendKeys(computerName);
		log.debug("Filtering results by name: "+computerName);
		test.log(LogStatus.INFO, "Filtering results by name: "+computerName);
		Reporter.log("Filtering results by name: "+computerName);
	}
	
	public void addComputer()
	{
		
		landingPageLocators.addBtn.click();
		log.debug("Click on the Add new computer button");
		test.log(LogStatus.INFO, "Click on the Add new computer button");
		Reporter.log("Click on the Add new computer button");
	}
	
	
	public void search()
	{
		
		landingPageLocators.filterBtn.click();
		log.debug("Click on the filter by name button");
		test.log(LogStatus.INFO, "Click on the filter by name button");
		Reporter.log("Click on the filter by name button");
	}

	
	public void goToNextResultPage()
	{
		
		landingPageLocators.nextPageLnk.click();
		log.debug("Going to next results page");
		test.log(LogStatus.INFO, "Going to next results page");
		Reporter.log("Going to next results page");
	}
	
	public void goToPreviousResultPage()
	{
		
		landingPageLocators.filterBtn.click();
		log.debug("Going to previous results page");
		test.log(LogStatus.INFO, "Going to previous results page");
		Reporter.log("Going to previous results page");
	}
	
	
	public String retrieveMessage()
	{
		String message = landingPageLocators.deleteMessage.getText();
		
		return message;
	}
	
	public int retrieveNumberOfComputers()
	{
		String temp = landingPageLocators.numberOfComputers.getText();
		String[] splited = temp.split("\\s+");
		int numberOfComputers = Integer.parseInt(splited[0]);
		return numberOfComputers;
	}
	
	
	
	public String retrieveCurrentPageText()
	{
		String currentPageText = landingPageLocators.currentPageText.getText();
		return currentPageText;
	}
	
	
	public void invertSortingByComputerName()
	{
		landingPageLocators.sortByComputerNameLnk.click();
		log.debug("Invert sorting by computer name");
		test.log(LogStatus.INFO, "Invert sorting by computer name");
		Reporter.log("Invert sorting by computer name");
	}
	
	
	public void invertSortingByIntroductionDate()
	{
		landingPageLocators.sortByDateIntroduced.click();
		log.debug("Invert sorting by introduction date");
		test.log(LogStatus.INFO, "Invert sorting by introduction date");
		Reporter.log("Invert sorting by introduction date");
	}
	
	public void invertSortingByDiscontinuedDate()
	{
		landingPageLocators.sortByDateDiscontinued.click();
		log.debug("Invert sorting by discontinued date");
		test.log(LogStatus.INFO, "Invert sorting by discontinued date");
		Reporter.log("Invert sorting by discontinued date");
	}
	
	public void invertSortingByCompany()
	{
		landingPageLocators.sortByDateCompanyName.click();
		log.debug("Invert sorting by company name");
		test.log(LogStatus.INFO, "Invert sorting by company name");
		Reporter.log("Invert sorting by company name");
	}
	
	
	public String retrieveFirstResultName()
	{
		String firstComputer = landingPageLocators.computerNameResult1.getText();
		return firstComputer;
	}
	
	
	public String retrieveSecondResultName()
	{
		
		String secondComputer = landingPageLocators.computerNameResult2.getText();
		return secondComputer;
	}

	public String retrieveFirstIntroducedDate()
	{
		String firstIntroDate = landingPageLocators.introducedDateResult1.getText();
		return firstIntroDate;
		
	}
	
	
	public String retrieveSecondIntroducedDate()
	{
		
		String secondIntroDate = landingPageLocators.introducedDateResult2.getText();
		return secondIntroDate;
	}

	
	public String retrieveFirstDiscontinuedDate()
	{
		

		String firstDiscoDate = landingPageLocators.discontinuedDateResult1.getText();
		return firstDiscoDate;
	}
	
	
	public String retrieveSecondDiscontinuedDate()
	{
		

		String secondDiscoDate = landingPageLocators.discontinuedDateResult2.getText();
		return secondDiscoDate;
	}

	
	public String retrieveFirstCompany()
	{
		
		String firstCompany = landingPageLocators.companyResult1.getText();
		return firstCompany;
	}
	
	
	public String retrieveSecondCompany()
	{
		
		String firstCompany = landingPageLocators.companyResult2.getText();
		return firstCompany;
	}
	
	public void editComputer(String computerName)
	{
		clickLink(computerName);
		log.debug("Editing computer with name: "+computerName);
		test.log(LogStatus.INFO, "Editing computer with name: "+computerName);
		Reporter.log("Editing computer with name: "+computerName);
	}
	
}
